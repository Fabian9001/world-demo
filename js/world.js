(()=>{
	
	"use strict";
	const KONTINENTE	= ["Europe", "Asia", "Africa", "North America", "South America", "Oceania", "Antarctica"];
	
	/**
	 * Essentially, a Selector contains a &lt;select&gt; element,<br/>
	 * and a string that will be part of the query string provided to PHP via fetch().<br/>
	 * A Selector may contain an <a href="Info.html">Info</a> object, which contains a &lt;dl&gt; element.<br/>
	 * Some methods will be overwritten when certain parameters are undefined.<br/>
	 * (Because both first and last Selector are different from the rest.)
	 * @class Selector
	*/
	class Selector {	
		/**
		 * @param {string} id see member description
		 * @param {string} querystr see member description
		 * @param {string} datakey see member description
		 * @param {Selector} components see member description
		 * @param {Info} info see member description
		*/
		constructor( id, querystr, datakey, components, info ) {
			{// standard Selector
				/**
				 * the &lt;select&gt; element this Selector refers to
				 * @const {HTMLSelectElement}
				 */
				this.element		= document.createElement('select');
				this.element.setAttribute('id', id);
				this.element.setAttribute('size', '7'); // put <select> element below label:
				document.querySelector(`label[for=${id}]`).parentNode.appendChild(this.element);
				/**
				 * inserted into the string template provided as an argument to fetch()
				 * @const {string}
				 */	
				this.querystr		= querystr; // constant (type String)
				/**
				 * to load its sub-components, each Selector (except the last) refers to another <a href="Selector.html">Selector</a>
				 * @const {Selector}
				 */
				this.components		= components; // constant (type Selector)
				/**
				 * refers to key in key-value-pairs in fetched and parsed data (see create_options method)
				 * @const {string}
				 */
				this.datakey		= datakey; // constant (type String)
				/**
				 * HTML ID refering to &lt;label&gt; and &lt;select&gt; element
				 * @const {string}
				 */
				this.id				= id; // constant (type String)
				/**
				 * some Selectors have <a href="Info.html">Info</a> members
				 * @const {Info}
				 */
				this.info			= info; // constant (type Info)
				/**
				 * Event Listener listening to scroll box elements being clicked.<br/>
				 * Passes option selected by user as an argument to query method.
				 * @param {Event} evt contains target.value which is an option selected by user<br/>
				 */
				this.eventlistener	= (evt) => { this.query(evt.target.value); }
				this.element.addEventListener( "change", this.eventlistener );
			}

			// last Selector (i.e. "city" Selector) does not break down into smaller components
			if( components === undefined ) { // event listener and creating options is different
				this.element.removeEventListener( "change", this.eventlistener );
				this.eventlistener = (evt) => {
					console.log(evt.target.value);
					this.info.query(evt.target.value);
				}
				this.element.addEventListener( "change", this.eventlistener );
				this.create_options = (data) => {
					data.forEach( (o) => {
						let option = document.createElement('option');
						option.value = o.ID; 
						option.textContent = o[this.datakey]; 
						this.element.appendChild(option);
					})
				} // components dummy:
				this.components = {
					change_style_to_abgeblendet_and_remove_children: () => {
						this.info.change_style_to_abgeblendet_and_remove_children();
					}
				}
			}
			
			// first Selector (i.e. "continent" Selector) has no querystr
			if( querystr === undefined ) { // creating options is different
				this.create_options = (data) => {
					data.forEach( continent => {
						let option = document.createElement('option');
						option.value = continent;
						option.textContent = continent;
						this.element.appendChild(option);
					} );
				}
			}
		}

		/**
		 * create &lt;option&gt; elements from data
		 * @param {Array} data fetched and parsed data
		 */
		create_options( data ) {
			data.forEach((o) => {
				let val = o[this.datakey];
				let option = document.createElement('option');
				option.value = val;
				option.textContent = val;
				this.element.appendChild(option);
			})
		}

		/**
		 * removes all children of a &lt;select&gt; element
		 */
		remove_children() {
			let last = this.element.lastChild;
			while(last) { // this.element.innerHTML = "";
				this.element.removeChild(last);
				last = this.element.lastChild;
			}
		}

		/**
		 * - calls function to write data to &lt;select&gt; element<br/>
		 * - removes 'loading' style from Selector
		 * @param {Array} data fetched and parsed data
		 */
		set_option_elements_and_remove_loading_style( data ) {
			this.create_options( data );
			document.querySelector(`label[for=${this.id}]`).classList.remove('loading');
		}
		
		change_style_to_abgeblendet_and_remove_children() {
			document.querySelector(`label[for=${this.id}]`).classList.add('abgeblendet');
			this.remove_children();
			if(this.info) this.info.change_style_to_abgeblendet_and_remove_children();
			this.components.change_style_to_abgeblendet_and_remove_children();  // "chain reaction"
		}
		
		/**
		 * - changes style from abgeblendet to loading in this (and in this.<a href="Info.html">Info</a> member)<br/>
		 * - removes children in this (and in this.<a href="Info.html">Info</a> member)<br/>
		 * - changes style to abgeblendet+removes children in its sub-component(s) (i.e., sub-Selector(s))
		 */
		change_style_and_remove_children() {
			let labelclass = document.querySelector(`label[for=${this.id}]`).classList
			labelclass.remove('abgeblendet');
			labelclass.add('loading');
			this.remove_children();
			if(this.info) this.info.change_style_to_abgeblendet_and_remove_children();
			this.components.change_style_to_abgeblendet_and_remove_children();
		}

		/**
		 * - triggers changing its sub-components' style, which includes removing their &lt;option&gt; elements<br/>
		 * - puts option selected by user in template string, which is passaed as an argument to fetch()<br/>
		 * - triggers <a href="Info.html">Info</a> query, if <a href="Info.html">Info</a> member exists
		 * @param {string} targetvalue option selected by user will be inserted into template string, which is passed as an argument to fetch()
		 */
		query( targetvalue ) {
			console.log(targetvalue);
			this.components.change_style_and_remove_children();
			if(this.info) this.info.query(targetvalue);
			fetch(`data/liste-json.php?liste=${this.components.querystr}&suche=${targetvalue}`)
			.then((response) => {
				if(!response.ok)
					throw new Error(`Fehler beim Laden der Daten (id="${this.id}" ?liste=${this.components.querystr}&suche=${targetvalue}).`);
				else
					return response.text(); })
			.then((data) => { this.components.set_option_elements_and_remove_loading_style(JSON.parse(data).Daten); })
			.catch((err) => { console.log("error:", err.message, this.element); });
		}		
	}

	/**
	 * Some <a href="Selector.html">Selector</a> instances have <a href="Info.html">Info</a> instances as members.<br/>
	 * -> Stadtinfo and Landesinfo<br/>
	 * Most Info methods have the same name as <a href="Selector.html">Selector</a> methods and they do essentially the same.
	 * @class Info
	*/
	class Info {
		/**
		 * @param {string} divID ID of the &lt;div&gt; element which contains the &lt;dl&gt; element
		 * @param {string} querystr inserted into the string template provided as an argument to fetch() 
		 */
		constructor( divID, querystr ) {
			this.element	= document.createElement('dl');
			document.querySelector(`div[id=${divID}]`).appendChild(this.element);	
			this.id			= divID;
			this.querystr	= querystr;
		}
		
		set_dtdd_elements_and_remove_loading_style( data ) {
			for(let key in data) {
				let dt = document.createElement('dt');
				dt.textContent = key;
				let dd = document.createElement('dd');
				dd.textContent = data[key];
				this.element.appendChild(dt);
				this.element.appendChild(dd);
			}
			document.querySelector(`div[id=${this.id}]`).parentNode.classList.remove('loading');
		}

		remove_children() {
			let last = this.element.lastChild;
			while(last) { // this.element.innerHTML = "";
				this.element.removeChild(last);
				last = this.element.lastChild;
			}
		}

		change_style_to_abgeblendet_and_remove_children() { 
			document.querySelector(`div[id=${this.id}]`).parentNode.classList.add('abgeblendet');
			this.remove_children();
		}

		change_style_to_loading_and_remove_children() {
			let parentdiv = document.querySelector(`div[id=${this.id}]`).parentNode
			parentdiv.classList.remove('abgeblendet');
			parentdiv.classList.add('loading');
			this.remove_children();
		}

		/**
		 * - changes its style and removes children from its &lt;dl&gt; element<br/>
		 * - puts option selected by user in template string, which is passaed as an argument to fetch()<br/>
		 * - fetches data which will be used to create children in &lt;dl&gt; element
		 * @param {string} targetvalue option selected by user will be inserted into template string, which is passed as an argument to fetch()
		 */
		query( targetvalue ) { 
			this.change_style_to_loading_and_remove_children();
			fetch(`data/liste-json.php?liste=${this.querystr}&suche=${targetvalue}`)
			.then((response) => {
				if(!response.ok)
					throw new Error(`Fehler beim Laden der Daten (id="${this.id}" ?liste=${this.querystr}&suche=${targetvalue}).`);
				else
					return response.text(); })
			.then((data) => { this.set_dtdd_elements_and_remove_loading_style(JSON.parse(data).Daten[0]); })
			.catch((err) => { console.log("error:", err.message, this.element); });
		}
	}

	// Selector initialization
	let cityinfo	= new Info( "ausgabe2", "stadtinfo" );
	let city		= new Selector( "stadt", "staedte", "Name", undefined, cityinfo );
	let landesinfo	= new Info( "ausgabe1", "landesinfo" );
	let country		= new Selector( "land", "laender", "Land", city, landesinfo );
	let region		= new Selector( "region", "regionen", "Region", country, undefined );
	let continent	= new Selector( "kontinent", undefined, undefined, region, undefined );
	continent.create_options( KONTINENTE );
})();
